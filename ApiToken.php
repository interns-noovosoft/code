<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class ApiToken
{
    public function handle($request, Closure $next)
    {
        // Get the user's API Token
        $apiToken = $request->bearerToken();

        // If user didn't send any token
        if (!$apiToken) {
            return response()->json(['message' => 'Unauthenticated'], 401);
        }

        // Get the user who sent the API Token
        $user = User::query()->where('api_token', '=', $apiToken)->first();

        if ($user) {
            $request->setUserResolver(function () use ($user) {
                return $user;
            });
            return $next($request);
        }

        return response()->json(['message' => 'Unauthenticated'], 401);
    }
}
