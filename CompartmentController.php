<?php

namespace App\Http\Controllers\Api;

use App\Domain\Compartment\Compartment;
use App\Domain\Compartment\CompartmentPrescription;
use App\Domain\Reminder\Reminder;
use App\Domain\Reminder\ReminderCompartment;
use App\Http\Controllers\Controller;
use App\Domain\Schedule\Schedule;
use Illuminate\Http\Request;

class CompartmentController extends Controller
{
    public function getAdvanceUntil()
    {
        $user = request()->user();
        $userId = $user->type == 'user' ? $user->id : $user->user_id;

        $advanceUntil = Reminder::query()
            ->where('user_id', '=', $userId)
            ->orderByDesc('reminder_date_time')
            ->first('reminder_date_time');

        return $advanceUntil;
    }
}
