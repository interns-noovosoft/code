<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginFormRequest;
use App\Http\Resources\UserResource;
use App\User;

class LoginController extends Controller
{
    public function postLogin(LoginFormRequest $request)
    {
        $data = $request->validated();

        // Get the user who is trying to login
        $user = User::query() 
            ->where('email', '=', $data['email'])
            ->first();

        // If there is no such user in our database    
        if (!$user) {
            return response()->json(['error' => 'invalid_credentials', 'message' => 'Invalid Username'], 401);
        }

        // Verify the credentials
        $credentials = array('email' => $data['email'], 'password' => $data['password']);

        if (($user && !auth()->attempt($credentials))) {
            return response()->json(['error' => 'invalid_credentials'], 401);
        }

        return $user;
    }
}
